import React, { useEffect, useState } from 'react';
export const CartContext = React.createContext({});

export const CartProvider = (props) => {

    const [cart, setCart] = useState([]);
    const [cartTotal, setCartTotal] = useState(0)


    function addCart(item, qtd) {

        let cardLock = cart.map((e) => e.title)
        console.log(cardLock)
        if (!cardLock.includes(item.title)) {
            setCart([...cart, {
                title: item.title,
                price: item.price,
                imageUrl: item.imageUrl,
                qtd: qtd,
                id: item.id,
                totalPrice: qtd * item.price
            }])
        } else {
            alert('ja esta no carrinho')
        }

    }

    function updateQtd(id, qtd) {
        if( qtd<=0){
            deleteProduct(id)
        }
        let ids = [];
        ids.push(id.id)
        let result = cart.reduce((acc, o) => acc.concat(ids.includes(o.id)
            ? Object.assign(o, { qtd: qtd, totalPrice: qtd * id.price }) :
            o), []);
            totalAmount()
    }

    function deleteProduct(item) {
        var deleteItem = cart.filter(function (f) { return f !== item })
        setCart(deleteItem)
    }


    function totalAmount() {
        let total = 0
        cart.map((qtd) => {
            total = total + qtd.totalPrice
        })
        setCartTotal(total)
    }

    useEffect(() => {
        totalAmount()
        
     }, [cart])



    return (

        < CartContext.Provider value={{ addCart, cart, updateQtd, deleteProduct, totalAmount, cartTotal }} >
            {props.children}
        </ CartContext.Provider>
    )
}

export const useCartProvider = () => React.useContext(CartContext)

