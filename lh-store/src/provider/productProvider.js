import React, { useState, useEffect } from 'react';
import { api } from '../util/api';




export const ProductContext = React.createContext({});

export const ProductProvider = (props) => {
    const [products, setProducts] = useState([]);
    const [filteredProducts, setFilteredProducts] = useState([]);
    const[singleProduct,setSingleProduct]= useState([])

    
    async function loadProducts() {
        try {
            let response = await api.get('products.json')
            let data = Object.entries(response.data);
            let filter = []
            let filterTwo = []
            data.map(id =>
                filter.push([{
                    id: id[1].id,
                    title: id[1].title,
                    isFavorite: id[1].isFavorite,
                    price: id[1].price,
                    imageUrl: id[1].imageUrl,
                    description: id[1].description,
                    category: id[1].category
                }])
            )
            filter.map(product=>product.map(item=>filterTwo.push(item)))
            setProducts(filterTwo)
        } catch (error) {
            console.log(error)
        }
    }

    function getByCategory(category){
      let filtered = products.filter(product=>product.category===category)
       setFilteredProducts(filtered)
    }

    function getSingleProduct(id){
     
        let filtered = products.filter(product=>product.id===id)
    
        setSingleProduct(filtered)
      }
    
    useEffect(() => {
        loadProducts()
    }, [])

    console.log(singleProduct)
    return (

        < ProductContext.Provider value={{ getSingleProduct, products,getByCategory,filteredProducts,singleProduct}} >
            {props.children}
        </ ProductContext.Provider>
    )
}

export const useProductProvider = () => React.useContext(ProductContext)

