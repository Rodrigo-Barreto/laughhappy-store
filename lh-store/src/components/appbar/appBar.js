import React from "react";
import { AppBar, Button, Grid, IconButton, Toolbar, Drawer, Box, Card, TextField } from "@material-ui/core";
import { useProductProvider } from "../../provider/productProvider";
import { useHistory } from "react-router";
import './style.css'
import { Close, Delete, DeleteOutline, FavoriteBorder, ShoppingCartOutlined } from "@material-ui/icons";
import { useCartProvider } from "../../provider/cartProvider";


function AppBarCustom() {

  const { getByCategory, singleProduct } = useProductProvider();
  const { cart, deleteProduct, totalAmount, cartTotal, updateQtd } = useCartProvider();
  let history = useHistory();
  const [state, setState] = React.useState({ right: false });
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };



  const toggleDrawer = (anchor, open) => (event) => {
    setState({ ...state, [anchor]: open });
  };


  return (
    <>

      {/* Cart */}
      <Drawer
        anchor="right"
        open={open}
        width='300'
      >

        <div className='closeDrawer'>
        <IconButton onClick={handleDrawerClose} >
          <Close>
          </Close>
        </IconButton>
        </div>
        
       
        <Box
          sx={{ width: "right" === "top" || "right" === "bottom" ? "auto" : 500 }}
          role="presentation"
        >
          {/* Cart Items */}
          {cart.map((item) =>
            <Card className="cardItem">
              <Grid container   >
                <Grid item xs={3} >
                  <img src={item.imageUrl} height='100' />
                </Grid>
                <Grid item xs={6}  >
                  {item.title}


                  <input
                    onChange={(event) => { updateQtd(item, event.target.value) }}
                    value={item.qtd}
                    label="Quantidade" type="number" min="0" max="5" />
                </Grid>
                <Grid item xs={3}>
                  <IconButton onClick={() => deleteProduct(item)}>
                    <DeleteOutline fontSize='large'></DeleteOutline  >
                  </IconButton>
                  <div>{eval(item.price * item.qtd)}</div>
                </Grid>
              </Grid></Card>
          )
          }

          <p>{cartTotal}</p>
          {/* Cart Items */}
        </Box>
      </Drawer>
      {/* Cart */}
      <AppBar>
        <Grid container className='topBar'  >
          <Grid item xs={3} className='logo'>
            <IconButton>
              <FavoriteBorder fontSize='large'></FavoriteBorder >
            </IconButton>
          </Grid>
          <Grid item xs={6} className='logo' >
            <img src='https://rihappynovo.vtexassets.com/arquivos/logo-rihappy-2d-solzinho-desk.png' alt='logo' onClick={() => { history.push("/") }} />
          </Grid>
          <Grid item xs={3}   >
            <IconButton>
              <ShoppingCartOutlined fontSize='large' onClick={handleDrawerOpen} ></ShoppingCartOutlined>
            </IconButton>
          </Grid>
        </Grid>



        <Grid container className='topBarDivider'  >
          <Grid item xs={2} className='item1'>
          </Grid>
          <Grid item xs={2} className='item2'>
          </Grid>
          <Grid item xs={2} className='item3'>
          </Grid>
          <Grid item xs={2} className='item4'>
          </Grid>
          <Grid item xs={2} className='item5'>
          </Grid>
          <Grid item xs={2} className='item6'>
          </Grid>
        </Grid>


        <Toolbar className='toolBarMenu'  >

          <Grid container  >
            <Grid item xs={2}   >
              <Button color="secondary" onClick={() => { getByCategory('bonecas'); history.push("/category/especial/bonecas") }}> <h4>Bonecas</h4></Button>
            </Grid>
            <Grid item xs={2} >
              <Button color="secondary" onClick={() => { getByCategory('carrinhos'); history.push("/category/carrinhos") }}><h3>Carrinhos</h3></Button>
            </Grid>
            <Grid item xs={2} >
              <Button color="secondary" onClick={() => { getByCategory('tabuleiro'); history.push("/category/tabuleiros") }}><h3>Tabuleiro</h3></Button>
            </Grid>
            <Grid item xs={2} >
              <Button color="secondary" onClick={() => { getByCategory('bonecos-colecionada'); history.push("/category/colecionaveis") }}><h3>Colecionaveis</h3></Button>
            </Grid>
            <Grid item xs={2} >
              <Button color="secondary" onClick={() => { getByCategory('star-wars'); history.push("/category/especial/star-wars") }}><h3>HotWells</h3></Button>
            </Grid>
            <Grid item xs={2} >
              <Button color="secondary" onClick={() => { getByCategory('star-wars'); history.push("/category/especial/star-wars") }}><h3>Star-wars</h3></Button>
            </Grid>

          </Grid>

        </Toolbar>
      </AppBar>
    </>
  );
}

export default AppBarCustom;




