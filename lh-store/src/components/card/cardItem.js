import React from "react";
import Card from "@material-ui/core/Card";
import { CardContent, CardHeader, CardMedia, IconButton, makeStyles, Typography, } from "@material-ui/core";
import { Favorite, FavoriteBorderOutlined } from "@material-ui/icons";
import './style.css'
import { useProductProvider } from "../../provider/productProvider";
import { useHistory } from "react-router";

const useStyles = makeStyles({
    card: {
        borderRadius:10,
        maxWidth: 230,
        margin: "auto",
        transition: "0.3s",
        boxShadow: "0 8px 40px -12px rgba(0,0,0,0.0)",
        "&:hover": {
            boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
        }
    },
}
)
function CardItem(props) {

    const{getSingleProduct}= useProductProvider();

    let history = useHistory()

    // const isFavorite = (id) => {
    //     let teste = props.favoritsStorage.map((e) => e.id)
    //     return !teste.includes(id)
    // }
    const classes = useStyles();
    return (
        <Card className={classes.card}  onClick={()=>{history.push(`/product/${props.product.id}`);}}   >
            <CardMedia

                className={classes.media}
                component="img"
                Height="180"
                image={props.product.imageUrl}
                alt="Paella dish"
            />
            <CardHeader
                 title={<Typography >{props.product.title}</Typography>}
                // action={<IconButton
                //     onClick={() =>
                //         props.function(props.products, props.index)}>
                //     {/* {isFavorite(props.products.id) ? <FavoriteBorderOutlined></FavoriteBorderOutlined> : <Favorite />} */}
                // </IconButton>}
            />


            <CardContent>

              
                <h2 className={classes.price}> R$ {props.product.price}</h2 >
                <Typography >
                    {props.product.description}
                </Typography>
            </CardContent>
        </Card>


    )

}
export default CardItem;