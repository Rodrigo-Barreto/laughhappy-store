import React from 'react';
import './App.css';
import AppBarCustom from './components/appbar/appBar';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import CategoryView from './views/category/handleCategory/handleCategoryView';
import HomeView from './views/home/homeView';
import SingleProductView from './views/singleProduct/singleProduct';



function App() {

  return (
    <>

      <Router>
        <AppBarCustom></AppBarCustom>
        <Switch>

        <Switch>
          <Route path="/category">
          <CategoryView></CategoryView>
          </Route>
          <Route path="/product/:id">
          <SingleProductView></SingleProductView>
          </Route>
          <Route path="/">
          <HomeView></HomeView>
          </Route>
        </Switch>
      
    
        </Switch>
      </Router>
      
    </>

  );
}

export default App;


// let teste = getByCategory('bonecas')
// return (
//   <div className="App">
//   {/* filtredProduct */}
//   { teste.map(product=><p>{product.title}</p>)} 
//   {/* AllPRoducts*/}
//   {/* { products.map(product=><p>{product.title}</p>)}  */}


//   </div>