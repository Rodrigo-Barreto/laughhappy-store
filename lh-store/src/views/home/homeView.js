
import { Box, Grid } from "@material-ui/core";
import React from "react";
import CardItem from "../../components/card/cardItem";
import { useProductProvider } from "../../provider/productProvider";


export default  function HomeView(){
    const { products } = useProductProvider();

    return(
        <div>
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={1}>
                </Grid>
            </Box>
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={1}>
                    {products.map((item, index) =>
                        <Grid item md={3} xs={12} sm={6}>
                            <CardItem index={index}
                                product={item} />
                        </Grid>
                    )}
                </Grid>
            </Box>
        </div>)
}
