
import { Button, Grid, TextField } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { useCartProvider } from "../../provider/cartProvider";
import { useProductProvider } from "../../provider/productProvider";
import './style.css'


export default function SingleProductView() {
    const { id } = useParams();
    const { getSingleProduct, singleProduct } = useProductProvider();
    const { addCart, updateQtd } = useCartProvider();
    const [qtd, setQtd] = useState(1);

    useEffect(() => {
        getSingleProduct(id)
    }, [])

    return (
        <div>
            <Grid container>

                {singleProduct.map((item) => <>
                    <Grid item xs={6} className='img' >
                        <img src={item.imageUrl} alt={item.id}></img>
                    </Grid>
                    <Grid item xs={6} className='' >
                        <h1>{item.title}</h1>
                        <h4>Categoria: {item.category}</h4>
                        <h2>R${item.price}</h2>
                        <h4>{item.description}</h4>
                        <input
                            onChange={(event) => { setQtd(event.target.value); updateQtd(item, event.target.value) }}

                            value={qtd}
                            label="Quantidade" type="number" min="1" max="10" />
                        {/* <TextField
                            min="1"
                            onChange={(event) => { setQtd(event.target.value); updateQtd(item, event.target.value) }}
                            value={qtd}
                            label="Quantidade"
                            type="number"
                            InputLabelProps={{
                                shrink: true,
                                
                            }}
                        /> */}
                        <div><Button onClick={() => addCart(item, qtd)}>Add ao Carrinho</Button></div>

                    </Grid>
                </>)}
            </Grid>

        </div>)
}
